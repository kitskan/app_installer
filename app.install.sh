#!/bin/bash

export APP_NAMES=(
Update
Curl
Timeshift
Git
ZSH
Flameshot
OpenVPN
Docker
docker-compose
)

export APPS=(
# update
update_upgade
# curl
curl_install
# timeshift
timeshift_install
# git
git_install
# zsh
zsh_install
# flameshot
flameshot_install
# openvpn
openvpn_install
# docker
docker_install
# docker-compose
docker_compose_install
)

# Color variable
RED=$(printf '\033[31m')
GREEN=$(printf '\033[32m')
YELLOW=$(printf '\033[33m')
BLUE=$(printf '\033[34m')
BOLD=$(printf '\033[1m')
RESET=$(printf '\033[m')

RESTART=false
TIMESLEEP=15

# Hard code
OS_NAME=$(. /etc/os-release; echo "$NAME")

# Echo 

# Update the apt package and install upgrade
update_upgade() {

    echo " Update the apt package and install upgrade ..."

    sudo apt update && sudo apt upgrade -y
}

# Install Curl
curl_install() {
	if [ -x "$(command -v curl)" ]; then
		echo "${GREEN}Curl already exists on this host ...${RESET}"
	else 
		sudo apt install curl -y

		if [ ! -z "$(command -v curl)" ]; then
			echo "${GREEN}Curl installation success complete!${RESET}"
		fi
	fi
}

# Install Timeshift

timeshift_install() {
	
	if [ -x "$(command -v timeshift)" ]; then
                echo "========================================="
                echo "${GREEN}Timeshift already exists on this host ...${RESET}"
                echo "========================================="
        else 
                sudo add-apt-repository ppa:teejee2008/ppa -y && \
				sudo apt update && \
				sudo apt install timeshift -y

                if [ ! -z "$(command -v timeshift)" ]; then
                        echo "${GREEN}Timeshift installation success complete!${RESET}"
                fi
        fi
}

# Install Git
git_install() {
        
        if [ -x "$(command -v git)" ]; then
                echo "========================================="
                echo "${GREEN}Git already exists on this host ...${RESET}"
                echo "========================================="
        else 
                sudo add-apt-repository ppa:git-core/ppa -y && \
                sudo apt update && \
                apt install git -y

                if [ ! -z "$(command -v git)" ]; then
                        echo "${GREEN}Git installation success complete!${RESET}"
                fi
        fi
}

#Install ZSH
zsh_install() {
	
	if [ -x "$(command -v zsh)" ]; then
		echo "${GREEN}ZSH already exists on this host ...${RESET}"
	else 
		sudo apt install zsh -y && \
		sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

		if [ ! -z "$(command -v zsh)" ]; then
			echo "${GREEN}ZSH installation success complete!${RESET}"
		fi
	fi
}

# Install Flameshot
flameshot_install() {
	
	if [ -x "$(command -v flameshot)" ]; then
		echo "${GREEN}Flameshot already exists on this host ...${RESET}"
	else 
		sudo apt install flameshot -y

		if [ ! -z "$(command -v flameshot)" ]; then
			echo "${GREEN}Flameshot installation success complete!${RESET}"
		fi
	fi
	
}

# Install OpenVPN
openvpn_install() {
        
	if [ -x "$(command -v openvpn)" ]; then
		echo "${GREEN}OpenVPN already exists on this host ...${RESET}"
	else 
		sudo apt install openvpn network-manager-openvpn network-manager-openvpn-gnome -y

		if [ ! -z "$(command -v openvpn)" ]; then
			echo "${GREEN}OpenVPN installation success complete!${RESET}"
		fi
	fi

}

# Install Docker
docker_install() {
    
	if [ -x "$(command -v docker)" ]; then
		echo "========================================="
		echo "${GREEN}Docker already exists on this host ...${RESET}"
        echo "========================================="
	else 
		# Logout is active.
		LOGOUT=1
		# Install packages to allow apt to use a repository over HTTPS:
    
		echo "Install packages to allow apt to use a repository over HTTPS ..."
    
		sudo apt install -y \
		apt-transport-https \
		ca-certificates \
		gnupg-agent \
		software-properties-common

		# Add Docker’s official GPG key:

		echo "Add Docker’s official GPG key ..."
    
		curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

		# Use the following command to set up the stable repository. Add stable repository Docker.
    
		echo "Add stable repository Docker ..."
		
		if [ $OS_NAME == "Ubuntu" ]; then

		# For Ubuntu
			echo "Ubuntu add repository"
			sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
		elif [ $OS_NAME == "Linux Mint" ]; then
			# For Mint
			echo "Linux Mint add repository"
			sudo add-apt-repository "deb https://download.docker.com/linux/ubuntu $(. /etc/os-release; ${UBUNTU_CODENAME}) stable"
		fi
		
		# Update the apt package index.
		echo "Update the apt package index ..."
		
		sudo apt update

		# Install the latest version of Docker

		echo "Install the latest version of Docker ..."
    
		sudo apt install docker-ce docker-ce-cli containerd.io -y

		# List the versions available in your repo:

		echo "List the versions available in your repo ..."
    
		apt-cache madison docker-ce

		# Adding your user to the docker group.

		echo "Adding your user to the Docker group ..."
		
		sudo groupadd docker && \
		sudo usermod -a -G docker ${USER}

			if [ ! -z "$(command -v docker)" ]; then
                echo "${GREEN}Docker installation success complete!${RESET}"
            fi

	fi

}

# Install Docker Compose
docker_compose_install() {
	if [ -x "$(command -v docker-compose)" ]; then
	
		echo "${GREEN}Docker-compose already exists on this host ...${RESET}"
	
	else

	# Download the current stable release of Docker Compose
	echo "Download the current stable release of Docker Compose ..."
	sudo curl -L "https://github.com/docker/compose/releases/download/1.25.3/docker-compose-$(uname -s)-$(uname -m)" \
	-o /usr/local/bin/docker-compose

	# Apply executable permissions to the binary
	"Apply executable permissions to the binary ..."
	sudo chmod +x /usr/local/bin/docker-compose

	# Create a symbolic link to /usr/bin
	echo "Create a symbolic link at /usr/bin ..."
	sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

	# docker-compose version
	echo "Docker-compose version ..."
	docker-compose --version

		if [ ! -z "$(command -v docker-compose)" ]; then
            echo "${GREEN}Docker Compose installation success complete!${RESET}"
        fi

	fi
}


IDX=0
for i in "${APP_NAMES[@]}"
do
        if [[ ${APP_NAMES[$IDX]} == ${1} ]] || [[ -z ${1} ]]; then
                
                APP_NAME=${APP_NAMES[$IDX]}
                APP=${APPS[$IDX]}

                echo "Application name: " ${BOLD} ${APP_NAME} ${RESET}
                
                $APP
        fi
        
        IDX=$IDX+1
done


# if [ $RESTART ]; then

# 	echo "Restart computer after {$TIMESLEEP} seconds and back in for this to take effect! ..."

# 	sleep $TIMESLEEP

# 	shutdown -r now

# else

	echo "${RED}For all changes to take effect, restart the computer!${RESET}"

# fi
